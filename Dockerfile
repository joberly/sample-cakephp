# Sample CakePHP App Deployment Image

FROM joberly/php-deploy:7.4.0

# Install composer dependencies
COPY composer.json /var/www/html/composer.json

RUN cd /var/www/html && \
    composer install --prefer-dist --no-scripts --no-dev --no-autoloader && \
    rm -rf .composer

# Move the Dockerfile to root so it is easy to find
COPY Dockerfile /Dockerfile

# Finish updating autoloader with classes
RUN cd /var/www/html && \
    composer dump-autoload --no-scripts --no-dev --optimize
