# sample-cakephp

A sample sandbox CakePHP web site with some automated deployment.

[![CircleCI](https://circleci.com/bb/joberly/sample-cakephp/tree/master.svg?style=svg)](https://circleci.com/bb/joberly/sample-cakephp/tree/master)

## CI/CD Setup

Deploys to [AWS Elastic Beanstalk](http://sample-cakephp-dev.m2fbszdqm9.us-west-2.elasticbeanstalk.com/).
Some PHP issues but the CI/CD process basically works.

### CircleCI dev Workflow

See [config.yml](.circleci/config.yml) for details.

- build: just downloads dependencies to do unit testing
- build-image: creates a deployment archive containing all the code and some deployment configuration files
- deploy-dev: copies image to S3, creates a new application version with it, then updates the `sample-cakephp-dev`
  environment to run the new application version. Uses the CircleCI aws-cli orb to access AWS. AWS access keys are
  stored in the CircleCI project environment variables.

### EB Application

Runs as a Multiple Docker type application with the following containers:

- php-app container: Runs PHP FPM from 
  [custom php-fpm image joberly/php-deploy:7.4.1](https://hub.docker.com/r/joberly/php-deploy)
  [(Dockerfile)](https://bitbucket.org/joberly/docker/src/master/php-deploy/7.4.1/Dockerfile) with the intl plugin installed.
  This image is based on the [vanilla PHP image php:7.4.1-fpm image](https://hub.docker.com/_/php).
- nginx-proxy: Uses [vanilla nginx Docker image nginx:1.17](https://hub.docker.com/_/nginx).

### TODO

- Fix PHP issues. These may be caused by not installing dev dependencies in deployment archive.
- Staging environment setup for deploying to separate staging environment from release branch.
- Production environment manual deployment from staging via CircleCI.
- More fine grained AWS access control for EB deployment (apparently this is complicated).

**NOTE: Documentation below is from the Composer skeleton. Left here as a reference for now.**

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.

If Composer is installed globally, run

```bash
composer create-project --prefer-dist cakephp/app
```

In case you want to use a custom app dir name (e.g. `/myapp/`):

```bash
composer create-project --prefer-dist cakephp/app myapp
```

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Update

Since this skeleton is a starting point for your application and various files
would have been modified as per your needs, there isn't a way to provide
automated upgrades, so you have to do any updates manually.

## Configuration

Read and edit `config/app.php` and setup the `'Datasources'` and any other
configuration relevant for your application.

## Layout

The app skeleton uses a subset of [Foundation](http://foundation.zurb.com/) (v5) CSS
framework by default. You can, however, replace it with any other library or
custom styles.
