#!/bin/bash

print() {
    echo "build-archive.sh: $@"
}

WORKSPACE=$1
APP_NAME="sample-cakephp"
ARCHIVE_NAME=${APP_NAME}.zip
ARCHIVE_WORKDIR=$WORKSPACE/$APP_NAME
ARCHIVE_PATH=$WORKSPACE/$ARCHIVE_NAME

print "Building deployment archive in $WORKSPACE"

# List of paths to copy to deployment archive
declare -a ARCHIVE_LIST_PHP_APP=(
    "app"
    "bin"
    "config"
    "plugins"
    "src"
    "vendor"
    "webroot"
    ".htaccess"
    "composer.json"
    "composer.lock"
    "index.php")

print "Creating archive working directory $ARCHIVE_WORKDIR"
[ -f $ARCHIVE_PATH ] && rm -f $ARCHIVE_PATH
[ -d $ARCHIVE_WORKDIR ] && rm -rf $ARCHIVE_WORKDIR
mkdir -p $ARCHIVE_WORKDIR/php-app

print "Copying PHP app files to working directory"
for item in ${ARCHIVE_LIST_PHP_APP[@]}; do
    cp -ar $item $ARCHIVE_WORKDIR/php-app/$item
done

print "Copying other deployment files to working directory"
cp -ar deploy/proxy $ARCHIVE_WORKDIR
cp deploy/Dockerrun.aws.json $ARCHIVE_WORKDIR

print "Creating deployment archive $ARCHIVE_NAME"
cd $ARCHIVE_WORKDIR ; zip -r $ARCHIVE_PATH * ; cd -

print "Removing archive working directory $ARCHIVE_WORKDIR"
rm -rf $ARCHIVE_WORKDIR

print "Done"
